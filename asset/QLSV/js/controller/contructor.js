function SinhVien(maSv, tenSv, email, matKhau, diemToan, diemLy, diemHoa) {
  this.ma = maSv;
  this.ten = tenSv;
  this.email = email;
  this.matKhau = matKhau;
  this.toan = diemToan;
  this.ly = diemLy;
  this.hoa = diemHoa;
  this.DTB = function () {
    const result = (this.diemToan * 1 + this.diemLy * 1 + this.diemHoa * 1) / 3;
    return result.toFixed(1);
  };
}
