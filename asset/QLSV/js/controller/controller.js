let layThongTinTuForm = function () {
  const maSv = document.getElementById("txtMaSV").value.trim();
  const tenSv = document.getElementById("txtTenSV").value.trim();
  const email = document.getElementById("txtEmail").value.trim();
  const matKhau = document.getElementById("txtPass").value.trim();
  const diemToan = document.getElementById("txtDiemToan").value.trim();
  const diemLy = document.getElementById("txtDiemLy").value.trim();
  const diemHoa = document.getElementById("txtDiemHoa").value.trim();

  return (sv = new SinhVien(
    maSv,
    tenSv,
    email,
    matKhau,
    diemToan,
    diemLy,
    diemHoa
  ));
};

let showThongTinLenForm = function (sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
};

let resetForm = function () {
  document.getElementById("formQLSV").reset();
};
