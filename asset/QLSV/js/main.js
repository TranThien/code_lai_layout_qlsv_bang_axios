let BASE_URL = "https://633ec05e83f50e9ba3b761d9.mockapi.io";
// lấy danh sách sinh viên theo axios

// bật loading :
let batLoading = function () {
  document.getElementById("loading").style.display = "flex";
};
// tắt loading :
let tatLoading = function () {
  document.getElementById("loading").style.display = "none";
};

// Hàm API:
let fetchListSv = function () {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      // dũ liệu lấy về khi back-end trả về là data
      // console.log(res.data);
      let data = res.data;
      renderListSv(data);
      tatLoading();
    })
    .catch(function (err) {
      console.error(err);
      tatLoading();
    });
};
// gọi và cho chạy lần đầu
fetchListSv();

let renderListSv = (list) => {
  let contentHTML = "";
  list.forEach(function (sv) {
    contentHTML += `<tr>
    <td>${sv.ma}</td>
    <td>${sv.ten}</td>
    <td>${sv.email}</td>
    <td>0</td>
    <td>
    <button onclick="xoaSv(${sv.ma})"  class="btn btn-danger">Xóa</button>
    <button onclick="suaSv(${sv.ma})" class="btn btn-primary">Sửa</button>
    </td>
    </tr>`;
  });
  document.querySelector("#tbodySinhVien").innerHTML = contentHTML;
};
let xoaSv = (idSv) => {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${idSv}/`,
    method: "DELETE",
  })
    .then(function (res) {
      fetchListSv();
      Swal.fire("Xóa Thành Công");
      tatLoading();

      console.log("res", res);
    })
    .catch(function (err) {
      Swal.fire("Xóa Thất Bại");
      tatLoading();
      console.log("err", err);
    });
};
let themSv = () => {
  batLoading();
  let sv = layThongTinTuForm();
  console.log("sv", sv);
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: sv,
  })
    // khi thêm sv mới từ trên form cần gọi ra trường dữ liệu data
    .then(function (res) {
      fetchListSv();
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
      console.log("err", err);
    });
};

let suaSv = (idSv) => {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${idSv}/`,
    method: "GET",
  })
    .then(function (res) {
      fetchListSv();
      showThongTinLenForm(res.data);
      tatLoading();
      console.log("res", res);
    })
    .catch(function (err) {
      tatLoading();
      console.log("err", err);
    });
};
let capNhatSv = () => {
  batLoading();
  const sv = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv/${sv.ma}`,
    method: "PUT",
    data: sv,
  })
    // khi cập nhật sv mới cũng cần gọi lại data
    .then(function (res) {
      fetchListSv();
      tatLoading();
      resetForm();
    })
    .catch(function (err) {
      tatLoading();
      console.log("err", err);
    });
};
